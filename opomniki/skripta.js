

window.addEventListener('load', function() {
	//stran nalozena

	var gumb = document.querySelector("input[type='button']");
	gumb.addEventListener('click', obdelajKlik);
	function obdelajKlik(){
    	var polje = document.querySelector("#uporabnisko_ime");
    	var vrednost = polje.value;
		document.getElementById("uporabnik").innerHTML = vrednost;
		document.querySelector('.pokrivalo').style.visibility = "hidden";

	}
	var dodGumb = document.getElementById("dodajGumb");
	dodGumb.addEventListener('click',dodajOpomnik);
	function dodajOpomnik(){
		var naziv = document.getElementById("naziv_opomnika").value;
		var cas = document.getElementById("cas_opomnika").value;
		document.getElementById("naziv_opomnika").value = "";
		document.getElementById("cas_opomnika").value = "";
		document.getElementById("opomniki").innerHTML+="<div class='opomnik senca rob'><div class='naziv_opomnika'>"+naziv+"</div><div class='cas_opomnika'> Opomnik čez <span>"+cas+"</span> sekund.</div></div>";
	}
	
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
			
		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
			var ime = opomnik.querySelector(".naziv_opomnika").innerHTML;
			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
			if(cas==0){
				alert("Opomnik!\n\nZadolžitev "+ime+" je potekla!");
				var removeEl = opomnik;
				removeEl.parentElement.removeChild(removeEl);
			}
			else{
				cas--;
				casovnik.innerHTML = cas;
			}
			
		}
	}
	
	setInterval(posodobiOpomnike, 1000);

});
